import { Component, Optional, Self, SkipSelf } from '@angular/core';
import { LoggerService } from './logger.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    {
      provide: LoggerService,
      useValue: {
        prefix: 'App Component',
        log(message: string) {
          console.log(`${this.prefix}: ${message}`);
        }
      }
    }
    
  ],
})
export class AppComponent {
  constructor(
    // @Optional()
    // @Self()
    @SkipSelf()
    private logger: LoggerService
  ) {
    if (this.logger) {
      this.logger.log('AppComponets constructor init');
    }
  }
}
