import { Directive } from '@angular/core';
import { LoggerService } from './logger.service';

@Directive({
  selector: '[appParent]',
  providers: [
    {
      provide: LoggerService,
      useValue: {
        prefix: 'Parent directive',
        log(message: string) {
          console.log(`${this.prefix}: ${message}`);
        }
      }
    }
  ]
})
export class ParentDirective {

  constructor() { }

}
