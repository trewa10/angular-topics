import {
  AfterViewInit,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  Injector,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ChildComponent } from './components/child-component/child.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild('divRef')
  divRef!: ElementRef;

  @ViewChild('template')
  template!: TemplateRef<any>;

  @ViewChild('template2')
  template2!: TemplateRef<any>;


  @ViewChild('vc', { read: ViewContainerRef })
  vc!: ViewContainerRef;

  childComponent = ChildComponent;

  constructor(
    private hostElement: ElementRef,
    // private injector: Injector,
    // private r: ComponentFactoryResolver // не використовується
  ) {
    // let factory = this.r.resolveComponentFactory(ChildComponent);
    // let componentRef = factory.create(injector);
    // let hostView = componentRef.hostView;
  }

  ngOnInit(): void {
    console.log('VC OnInit: ', this.divRef?.nativeElement?.textContent); // на цьому етапі життєвого циклу елемент ще не доступний через @ViewChild
    console.log(
      'DI OnInit: ',
      this.hostElement.nativeElement.children[0].textContent
    ); // через DI є доступ до шаблону та його елементів
  }

  ngAfterViewInit(): void {
    console.log('VC AfterViewInit: ', this.divRef.nativeElement.textContent);
    console.log('DI AfterViewInit: ', this.hostElement.nativeElement);

    // TemplateRef
    let elementRef = this.template.elementRef;
    console.log(elementRef.nativeElement);

    // Creating an embedded view
    let view = this.template.createEmbeddedView(null);
    //adding view into ng-container
    console.log(view);
    
    this.vc.insert(view);
    // this.vc.detach(); // remove view from ng-container

    this.vc.createEmbeddedView(this.template2, {
      
    }, {
      index: 0
    }); // insted of create view and insert

    // ViewContainerRef
    console.log(this.vc.element.nativeElement);

    this.vc.createComponent(ChildComponent); //instead of resolveComponentFactory
  }
}
