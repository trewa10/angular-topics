import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.scss']
})
export class ChildComponentComponent {

  @Input()
  template?: TemplateRef<{$implicit: number}>;   //TemplateRef<any> TemplateRef<void>

}
