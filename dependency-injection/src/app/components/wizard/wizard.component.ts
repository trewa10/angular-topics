import { StepOne } from './../steps/step-one.component';
import { AfterViewInit, Component, ElementRef, QueryList, ViewChildren, ContentChildren, AfterContentInit } from '@angular/core';
import { WizardStepDirective } from 'src/app/directives/wizard-step.directive';

@Component({
  selector: 'wizard',
  template: `
  <ng-content></ng-content>
  <div #step *ngFor="let item of list" >This is step</div>
  
  `,
})
export class Wizard implements AfterViewInit, AfterContentInit {
  @ViewChildren('step')   
  steps!: QueryList<ElementRef>;

  @ContentChildren('childStep')
  childSteps!: QueryList<ElementRef>;

  @ContentChildren(WizardStepDirective, {read: ElementRef})
  wizardSteps!: QueryList<ElementRef>;
  
  list = [];


  ngAfterViewInit(): void {
    // console.log(this.steps);
    this.steps.changes.subscribe(() => {
      console.log(`There are now ${this.steps.length} steps.`);
      
    })
  }
  
  ngAfterContentInit(): void {
    console.log(this.childSteps);
    console.log(this.wizardSteps);
    
    
  }

  // method for steps
  public registerStep (step: StepOne) {

  }

}
