import { Component, forwardRef } from '@angular/core';
import { Wizard } from '../wizard/wizard.component';
import { WizardStepService } from 'src/app/services/wizard-step.service';

@Component({
  selector: 'step-one',
  template: '<h2>This is step one!</h2>',
  providers: [
    {provide: WizardStepService, useExisting: StepOne}, // працює і так
    // { provide: WizardStepService, useExisting: forwardRef(() => StepOne) }, // щоб уникнути помилки використання до ініціалізації
  ],
})
export class StepOne implements WizardStepService {
  constructor(wizard: Wizard) {
    wizard.registerStep(this);
  }

  show(): void {}

  hide(): void {}
}
