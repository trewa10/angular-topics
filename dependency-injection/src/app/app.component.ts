import { LoggerService } from './services/logger.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dependency-injection';

  constructor(
    loggerService: LoggerService
  ) {
    loggerService.info('Logger is working');
  }
}
