import { RETRIES } from './services/retry-interceptor';
import { RetryModule } from './services/retry/retry.module';
import { RetryService } from './services/retry.service';
import { BadAuthService } from './services/bad-auth.service';
import { AuthService } from './services/auth.service';
import { NewLoggerService } from './services/new-logger.service';
import { LoggerService } from './services/logger.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { Wizard } from './components/wizard/wizard.component';
import { WizardStepDirective } from './directives/wizard-step.directive';

@NgModule({
  declarations: [
    AppComponent,
    Wizard,
    WizardStepDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RetryModule.forRoot()
    // RetryModule.withRetries(2),  // перезапише значення. Замість запису нижче
    // RetryModule.castomizeDomain('domain.com', 4) // аналогічно як і вище
    
  ],
  providers: [
    // LoggerService,     // the same {provide: LoggerService, useClass: LoggerService}

    // // NewLoggerService,

    // {
    //   provide: LoggerService,
    //   useClass: NewLoggerService  // створює новий інстанс класу useExisting каже використовувати вже існуючий
    // },

    // // {
    // //   provide: LoggerService,
    // //   useExisting: NewLoggerService  // використовує вже існуючий інстанс класу 
    // // },

    // {
    //   provide: RetryService,
    //   useFactory: (authService: AuthService) => {
    //     return authService.getToken()  // уточнити як це повинно працювати
    //   },
    //   deps: [AuthService]
    // },

    // {provide: RETRIES, useValue: 2} // перезапише значення

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
