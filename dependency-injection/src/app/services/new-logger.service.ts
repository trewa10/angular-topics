import { LoggerService } from './logger.service';
import { Injectable } from '@angular/core';

@Injectable()
export class NewLoggerService extends LoggerService {
  override info(msg: string) {
    super.info('---' + msg + '---')
  }
}
