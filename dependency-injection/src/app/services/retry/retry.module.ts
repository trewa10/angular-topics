import { LoggerService } from './../logger.service';
import { RetryInterceptor, RETRIES, DOMAIN_RETRIES } from './../retry-interceptor';
import { ModuleWithProviders, NgModule, Self } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [              // providers for any injector
    // LoggerService  
  // {
    //   provide: HTTP_INTERCEPTORS, useClass: RetryInterceptor, multi: true
    // },
    // {
    //   provide: RETRIES, useValue: 3
    // },
    // {
    //   provide: DOMAIN_RETRIES,
    //   useValue: {domain: 'service.com', retries: 5},
    //   multi: true
    // }
  ]
})
export class RetryModule { 

  // Validate that HttpClient exist in the same injector
  constructor(@Self() client: HttpClient) {

  }

  static withRetries(defaultRetries: number): ModuleWithProviders<RetryModule> {
    return {
      ngModule: RetryModule,
      providers: [
        {provide: RETRIES, useValue: defaultRetries}
      ]
    }
  }

  static castomizeDomain(domain: string, retries: number): ModuleWithProviders<RetryModule> {
    return {
      ngModule: RetryModule,
      providers: [
        {provide: DOMAIN_RETRIES, useValue: {domain, retries}, multi: true}
      ]
    }
  }

  static forRoot(): ModuleWithProviders<RetryModule> {   // method to use in AppModule
    return {
      ngModule: RetryModule,
      providers: [              //providers only for the app injector
        LoggerService
      ]
    }
  }
}
