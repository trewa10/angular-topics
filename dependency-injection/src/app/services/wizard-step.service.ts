import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export abstract class WizardStepService {   // interactions between the wizard and its steps use this interface

  abstract show(): void;
  abstract hide(): void;
}
