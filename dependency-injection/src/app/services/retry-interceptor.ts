import { Injectable, InjectionToken, Inject, inject, Optional } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

export const RETRIES = new InjectionToken<number>('Default number of retries');

export interface DomainRetryLimit {
  domain: string;
  retries: number;
}

export const DOMAIN_RETRIES = new InjectionToken<DomainRetryLimit>('Retries per domain');

@Injectable()
export class RetryInterceptor implements HttpInterceptor {
  constructor(
    @Inject(RETRIES) retries: number,
    
    @Optional() @Inject(DOMAIN_RETRIES) domainRetries: DomainRetryLimit[] | null
    ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req);
  }
}
