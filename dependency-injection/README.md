# DependencyInjection

## Steps:
### DI
<!-- DI - розділити концепт залежності від чогось від знання як це створити -->
 1. Inerceptor <!-- services/retry-interceptor  -->
 2. Tokens <!-- RETRIES  -->
 3. useClass <!-- app.module  -->
 4. useExisting <!-- app.module  -->
 5. useFactory <!-- app.module || ??? -->
 6. multi
 7. useValue <!-- app.module  -->
 8. DOMAIN_RETRIES <!-- services/retry-interceptor  -->
 9. Модулі та перезапис значень в провайдері + модулі з методами
 10. метод forRoot <!-- retry.module  -->
 11. @self <!-- retry.module  -->
 12. Wizard (child & children)
